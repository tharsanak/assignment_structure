#include <stdio.h>
struct student {
    char firstName[50];
    char subject[50];
    float marks;
}s[5] ;

int main() {
    int i;
    printf("Enter information of students:\n");

    // storing information
    for (i = 1; i < 6; i++) {
    	printf("student%d\n",i);
        printf("Enter firstname: ");
        scanf("%s", s[i].firstName);
        printf("Enter subject: ");
        scanf("%s", s[i].subject);
        printf("Enter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("\nDisplaying Information:\n\n");

    // displaying information
    for (i = 1; i < 6; i++) {
    	printf("student%d\n",i);
        printf("firstname:%s\n",s[i].firstName);
        printf("subject:%s\n",s[i].subject);
        printf("Marks: %.2f\n", s[i].marks);
        printf("\n");
    }
    return 0;
}
